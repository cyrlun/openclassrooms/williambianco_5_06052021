

const cart = document.querySelector('tbody')
const orderRecup = sessionStorage.getItem("orderId")
const orderIdDiv = document.getElementById('order-id')
if (!orderRecup) {
    window.location.href = '/public'
}
const orderProducts = JSON.parse(orderRecup).products
let totalPrice = 0



/**
 * Fonction qui affiche le récapitulatif de la commande:
 *      - Affiche le numéro de commande
 *      - Créé le tableau des produits achetés avec le prix total
 *      - Affiche les coordonnées du client
 */

const showConfirm = () => {
    //Affichage du numéro de commande généré par l'API
    orderIdDiv.innerText = JSON.parse(orderRecup).orderId
    //Affichage des produits achetés sous forme de tableau
    for (let i = 0; i < orderProducts.length; i++) {
        const productsCart= sessionStorage.getItem("myCart")
        const cartContent = JSON.parse(productsCart)
        cart.innerHTML +=   `<tr>
                                <td class="name col-2 px-0">${cartContent[i].name}</td>
                                <td class="col-2 px-0">${cartContent[i].color}</td>
                                <td class="quantity number col-2 px-0">${cartContent[i].quantity}</td>
                                <td class="number col-2 px-0">${cartContent[i].price / 100},00€</td>
                            </tr>
                            `
        totalPrice += Number(cartContent[i].price)
        
    }
    //Affichage du prix total de la commande
    const showTotalPrice = document.querySelector('tfoot')
    showTotalPrice.innerHTML = `<td></td>
                                 <td></td>
                                 <td>Total</td>
                                 <td class="number col-3">${totalPrice / 100},00€</td>`
    //Affichage des coordonnées du client
    const orderContact = (JSON.parse(orderRecup)).contact
    const contact = document.getElementById('contact')
    
    contact.innerHTML = `<p>${orderContact.lastName} ${orderContact.firstName}</p>
                        <p>${orderContact.email}</p>
                        <p>${orderContact.address}</p>
                        <p>${orderContact.city}</p>`
}

showConfirm()
sessionStorage.clear()