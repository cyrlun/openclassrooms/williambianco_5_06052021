const url = (new URL(document.location)).searchParams
const id = url.get('id')
const getTeddy = `http://localhost:3000/api/teddies/${id}`

// Requête fetch(get) pour récupérer le produit sélectionné sur la page d'accueil

fetch(getTeddy)
  .then(response => response.json())
  .then(data => {
    showProduct(data)
    addCart(data)
  })
  .catch(err => console.log(err))


/** Fonction qui met en page le produit sélectionné
 * 
 * @param {object} teddy Réponse de l'API qui nous renvoie le produit sélectionné
 */
const showProduct = (teddy) => {
  const teddyPlace = document.getElementById('product-container')
  const teddyTitle = document.querySelector('title')

  // Création de la "carte produit" qui contient toutes les informations du produit sélectionné(excepté l'id) 
  // ainsi que les boutons de navigation 
  // (pour un retour à l'accueil, l'ajout du produit au panier et pour voir son panier)

  teddyTitle.innerText = `OrinoBears - ${teddy.name}`
  teddyPlace.innerHTML = `
                        <form>
                          <div class="d-flex justify-content-around">
                              <img src="${teddy.imageUrl}" alt="Photo de l'ourson ${teddy.name}" class =" ml-3 col-6">
                              <div class="col-5">
                                <h4 class="text-center col-12">${teddy.name}</h4>
                                <div class="form-group">
                                    <label for="colors">Couleurs</label>
                                    <select class="col-12" name="colors" id="colors">  
                                    </select>
                                </div>
                                <div class="text-center">
                                  <h4>Prix</h4>
                                  <p>${teddy.price / 100},00€</p>
                                </div>
                              </div>
                          </div>
                          <div class="description text-center col-12 my-4 px-0">
                            <h4 class="col-12 ">Description</h4>
                            <p class="description-text col-6">${teddy.description}</p>
                            <div class="d-flex justify-content-around align-items-center col-12">
                              <a href = "/public/index.html" class="btn btn-secondary bg-info" id="return"> < Continuer mes achats</a>
                              <a href = "#" class="btn btn-secondary bg-success" id="addCart">Ajouter au panier</a>
                              <a href = "/public/shop-cart.html" class="btn btn-secondary bg-primary" id="shop-cart">Voir mon panier > </a>
                            </div>
                            <div class="bg-success text-light col-12 mt-3 d-none form-appear" id="return-cart">Je suis content d'avoir trouvé un nouvel ami!</div>
                          </div>
                        </form>
`
  //Création des options de coloris pour le produit sélectionné

  for (let i = 0; i < teddy.colors.length; i++) {
    const color = teddy.colors[i];
    const select = document.getElementById('colors')
    const newOption = document.createElement('option')
    newOption.setAttribute('value', color)
    select.appendChild(newOption)
    newOption.innerText = `${teddy.colors[i]}`
  }
}

/** Fonction qui permet, au clic sur le bouton "Ajouter au panier", d'ajouter le produit sélectionné au panier
 * 
 * @param {object} teddy Réponse de l'API qui nous renvoie le produit sélectionné
 */

const addCart = (teddy) => {
  const addCart = document.querySelector('#addCart')
  addCart.addEventListener('click', function (event) {
    event.preventDefault()
    const returnCart = document.getElementById('return-cart')
    returnCart.classList.remove('d-none')
    setTimeout(() => {
      returnCart.classList.add('d-none')
    }, 1200)

    const myCart = sessionStorage.getItem("myCart") !== null ? JSON.parse(sessionStorage.getItem("myCart")) : []
    let teddySelected = {
      "name": teddy.name,
      "color": document.getElementById('colors').value,
      "quantity": 1,
      "price": `${(teddy.price)}`,
      "id": `${teddy._id}`
    }

    if (myCart.length != 0 && myCart != null) {
      const element = myCart.find(element => element.id == teddySelected.id && element.color == teddySelected.color)
      if (element) {
        console.log("objet déjà présent dans le panier");
        element.quantity++
        sessionStorage.setItem("myCart", JSON.stringify(myCart))
      } else {
        myCart.push(teddySelected)
      }
      sessionStorage.setItem("myCart", JSON.stringify(myCart))

    } else {
      myCart.push(teddySelected)
      sessionStorage.setItem("myCart", JSON.stringify(myCart))
    }
  })
}