const totalTable = document.querySelector('table')
const table = document.querySelector('tbody')
const showFormButton = document.querySelector('#fake-button')
const showTotalPrice = document.querySelector('tfoot')

const emptyCartButton = document.getElementById('empty-cart-button')
const emptyCartText = document.getElementById('empty-cart-text')
const successDeleteText = document.getElementById('success-delete')
const form = document.querySelector('#facturation-container').classList
const trueButton = document.querySelector('#true-button').classList

const firstNameInput = document.querySelector("#firstname")
const lastNameInput = document.querySelector("#lastname")
const mailInput = document.querySelector("#mail")
const addressInput = document.querySelector("#address")
const postalInput = document.querySelector("#postal")
const cityInput = document.querySelector("#city")
const phoneInput = document.querySelector("#phone")

const regexFirstName = /^[a-zA-Z- ]{2}/
const regexLastName = /^[a-zA-Z-]{2}/
const regexMail = /\S+@\S+\.\S+/
const regexAddress = /[a-zA-Z0-9- ]{8}/
const regexPostal = /[0-9]{5}/
const regexCity = /^[a-zA-Z- ]{6}/

const allImg = document.querySelectorAll('.icone-verif')
const allSpan = document.querySelectorAll('span')

/**
 * Fonction qui permet de vider le panier
 */
const deleteItems = () => {
    sessionStorage.clear()
    totalTable.classList.add('d-none')
    emptyCartText.classList.remove('d-none')
    showFormButton.classList.add('d-none')
    emptyCartButton.classList.add('d-none')
    form.add('d-none')
    trueButton.add('d-none')
    showFormButton.classList.add('d-none')
}
function arrayRemove(array, value){
    return array.filter(function(ele){ 
        return ele != value; 
    });
}

/**
 * Fonction qui affiche dans le panier l'article sélectionné sur la page produit et qui affiche le prix total de la commande
 */
const showShopCart = () => {

    
    const myCart = getMyCart()
    if (myCart != null && myCart.length != 0) {
        showFormButton.classList.remove('d-none')
        totalTable.classList.remove('d-none')
        emptyCartText.classList.add('d-none')
        myCart.forEach((element, i) => {
            const btn = document.createElement('button')
            btn.setAttribute('id','btn-delete-one')
            btn.classList.add('btn', 'btn-danger', 'text-light', 'p-0', 'm-1')
            btn.textContent = "X"
            btn.dataset.index = i
            const tr = document.createElement('tr')
            tr.classList.add('col-2', 'name','px-0')
            const td1 = document.createElement('td')
            td1.textContent = element.name
            td1.classList.add('col-2','col-md-3','px-0')
            const td2 = document.createElement('td')
            td2.classList.add('col-2','col-md-3','px-0')
            td2.textContent = element.color
            const td3 = document.createElement('td')
            td3.classList.add('col-2','col-md-3', 'quantity', 'number','px-0','px-md-3')
            const td3Input = document.createElement('input')
            td3Input.classList.add('col-12','col-md-6','text-right','px-0')
            td3Input.setAttribute('type','number')
            td3Input.setAttribute('min',1)
            td3.appendChild(td3Input)
            td3Input.value = element.quantity
            const td4 = document.createElement('td')
            td4.classList.add('col-2','col-md-3','number')
            td4.textContent = `${(element.price * element.quantity) / 100},00€`
            const td5 = document.createElement('td')
            td5.classList.add('col-2','px-md-1')
            td5.appendChild(btn)
            

            tr.append(td1, td2, td3, td4, td5)
            table.appendChild(tr)
            btn.onclick = () => {
                successDeleteText.innerText = `${element.name} a bien été supprimé du panier!`
                successDeleteText.classList.remove('d-none')
                setTimeout(() => {
                    successDeleteText.classList.add('d-none')
                },3000)
                tr.classList.add('d-none')
                console.log(myCart);
                myCart.splice(i,1)
                console.log(myCart.splice(i,1));
                sessionStorage.setItem("myCart", JSON.stringify(myCart))
                setTotalPrice()
                if (myCart == null || myCart.length == 0) { 
                    totalTable.classList.add('d-none')
                    showFormButton.classList.add('d-none')
                    emptyCartText.classList.remove('d-none')
                    emptyCartButton.classList.add('d-none')
                }
                
            }
            td3Input.addEventListener('input',() => {
                element.quantity = td3Input.value
                td4.textContent = `${(element.price * element.quantity) / 100},00€`
                sessionStorage.setItem('myCart',JSON.stringify(myCart))
                
                setTotalPrice()
            })
        }); 
    }else {   
        // console.log("panier vide");
        emptyCartButton.classList.add('d-none')
        totalTable.classList.add('d-none')
        showFormButton.classList.add('d-none')
        emptyCartText.classList.remove('d-none')
        emptyCartButton.classList.add('d-none')
    }
    
    showTotalPrice.innerHTML = `<td></td>
                                <td></td>
                                <td>Total</td>
                                <td class="number col-2" id="total-price"></td>
                                <td></td>
                                `
    setTotalPrice()
    
}


// fonction qui récupère le panier
const getMyCart = () => sessionStorage.getItem("myCart") !== null ? JSON.parse(sessionStorage.getItem("myCart")) : []

const getTotalPrice = () =>{
    const myCart = getMyCart()
    let total = 0
    myCart.forEach(element => {
        // console.log(myCart)
        total += element.price * element.quantity
    })
    return total
}
/**
 * Fonction qui calcule le prix total du panier
 */
 const setTotalPrice = () => {
    console.log(getTotalPrice());
    const tdTotalPrice = document.getElementById('total-price')
    tdTotalPrice.innerText = `${getTotalPrice()/ 100},00 €`
}
const getProductsId = () => {
    const myCart = getMyCart()
    return myCart.map((element) => element.id)
}




/**
 * Fonction qui créé l'objet de la requête Post au format attendu (objet de contact et tableau de produits)  et l'envoie à l'API
 * @returns un objet contact, un tableau de produits et un orderId (de type string)
 */
function sendFormRequest() {
    const order = {
        contact: {
            firstName: firstNameInput.value,
            lastName: lastNameInput.value,
            address: addressInput.value,
            city: (postalInput.value + " " + cityInput.value),
            email: mailInput.value
        },
        products: getProductsId()
    }
    const postTeddies = `http://localhost:3000/api/teddies/order`
    const configPost = {
        method: "POST",
        headers: new Headers({
            "Accept": "application/json",
            "Content-Type": "application/json"
        }),
        body: JSON.stringify(order)
    }
    fetch(postTeddies, configPost)
        .then((response) => response.json())
        .then((data) => sessionStorage.setItem("orderId", JSON.stringify(data)))
        .catch((err) => console.log(err))
}


// VERIFICATION DU FORMULAIRE

/**
 * Fonction qui permet de vérifier les données saisies par l'utilisateur
 * @param {object} input Input ciblé
 * @param {object} regex Expression régulière permettant de vérifier l'input ciblé
 * @param {number} index Ordre dans lequel est affiché l'input sur la page HTML
 */

const testFormConform = (input, regex, index) => {
    if (regex === regexFirstName || regex === regexLastName) {
        
        input.addEventListener('input', (e) => {
            if (regex.test(e.target.value) && e.target.value.length >= 2) {
                allImg[index].classList.remove("d-none")
                allImg[index].src = "/images/check.svg"
                allSpan[index].classList.add("d-none")
            } else if (!regex.test(e.target.value) || e.target.value.length == 1) {
                allImg[index].classList.remove("d-none")
                allImg[index].src = "/images/error.svg"
                allSpan[index].classList.remove("d-none")
            } else {
                allImg[index].classList.remove('d-none')
            }
        })
    } else {
        input.addEventListener('input', (e) => {
            
            if (regex.test(e.target.value)) {
                allImg[index].classList.remove("d-none")
                allImg[index].src = "/images/check.svg"
                allSpan[index].classList.add("d-none")
            } else if (!regex.test(e.target.value)) {
                allImg[index].classList.remove("d-none")
                allImg[index].src = "/images/error.svg"
                allSpan[index].classList.remove("d-none")
            } else {
                allImg[index].classList.remove('d-none')
            }
        })
    }
}

testFormConform(firstNameInput, regexFirstName, 0)
testFormConform(lastNameInput, regexLastName, 1)
testFormConform(mailInput, regexMail, 2)
testFormConform(addressInput, regexAddress, 3)
testFormConform(postalInput, regexPostal, 4)
testFormConform(cityInput, regexCity, 5)

emptyCartButton.addEventListener('click', deleteItems)

const submitButton = document.querySelector("#true-button")

submitButton.addEventListener('click', (e) => {
    
    if ((firstNameInput.value && regexFirstName.test(firstNameInput.value)) &&
        (lastNameInput.value && regexLastName.test(lastNameInput.value)) &&
        (addressInput.value && regexAddress.test(addressInput.value)) &&
        (cityInput.value && regexCity.test(cityInput.value) && regexPostal.test(postalInput.value)) &&
        (mailInput.value && regexMail.test(mailInput.value))) {
        
        sendFormRequest()
    } else {
        e.preventDefault()
        const warning = document.getElementById('warning')
        warning.classList.remove('d-none')
        setTimeout(() => {
            warning.classList.add('d-none')
          },3000)
        if (!firstNameInput.value || !regexFirstName.test(firstNameInput.value)) {
            const warningFirstName = document.querySelector('#warning-firstname').classList
            warningFirstName.remove('d-none')
        }
        if (!lastNameInput.value || !regexLastName.test(lastNameInput.value)) {
            const warningLastName = document.querySelector('#warning-lastname').classList
            warningLastName.remove('d-none')
        }
        if (!mailInput.value || !regexMail.test(mailInput.value)) {
            const warningMail = document.querySelector('#warning-mail').classList
            warningMail.remove('d-none')
        }
        if (!addressInput.value || !regexAddress.test(addressInput.value)) {
            const warningAddress = document.querySelector('#warning-address').classList
            warningAddress.remove('d-none')
        }
        if (!postalInput.value || !regexPostal.test(postalInput.value)) {
            const warningPostal = document.querySelector('#warning-postal').classList
            warningPostal.remove('d-none')
        }
        if (!cityInput.value || !regexCity.test(cityInput.value)) {
            const warningCity = document.querySelector('#warning-city').classList
            warningCity.remove('d-none')
        }
    }
})

showFormButton.onclick = () => {
    const form = document.querySelector('#facturation-container').classList
    const trueButton = document.querySelector('#true-button').classList
    form.remove('d-none')
    trueButton.remove('d-none')
    showFormButton.classList.add('d-none')
}
showShopCart()