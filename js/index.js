// Déclaration des variables

const apiTeddies = 'http://localhost:3000/api/teddies'
const teddyCard = document.getElementById('container-teddies')

/**
 * Fonction qui récupère les données de l'API et appelle la fonction createCards()
 * @param {string} apiUrl Url de l'API (Ici l'API Teddies)
 * @returns void
 */

 const requestFetch = (apiUrl) => {
    fetch(apiUrl)
        .then((res) => {return res.json()})
        .then((value) => createCards(value))
        .catch((err) => console.log(err))
}
requestFetch(apiTeddies)

/**
 * Fonction qui créé une "carte produit" pour chaque produits renvoyé par l'API
 * Construction des éléments du DOM pour l'affichage des vignettes produits (1ere boucle for)
 * Affichage des couleurs disponibles pour chaque ourson dans la vignette (2eme boucle for)
 * @param {object} value Liste des produits récupérée via l'API
 */

const createCards = (value) =>{
//Création des vignettes
    for (let i = 0; i < value.length; i++) {
        teddyCard.innerHTML += `<a href="./product.html?id=${value[i]._id}" id='teddy-${i}' class='d-flex row  justify-content-center position-relative'>
                                <img src="../images/teddy_${i + 1}.jpg" alt="Photo de l'ours ${value[i].name}" class="round" id="img-teddy-${i}">
                                <div class="info-container col-12 text-center mx-0 mt-0 justify-content-center">
                                    <h2 class="position-absolute col-12">${value[i].name}</h2>
                                    <h3>${value[i].price / 100},00€</h3>
                                    <div id='teddy-color-${i}' class="col-12 justify-content-center d-flex"></div>
                                </div>
                            </a>`
        
//Création des pastilles de couleur dans les vignettes
        for (let j = 0; j < value[i].colors.length; j++) {
            const colorDiv = document.createElement('div')
            const teddy = document.getElementById(`teddy-color-${i}`)
            const color = `${value[i].colors[j]}`
// Conditionnement si la couleur n'est pas une couleur reconnue comme "existante"
            if (color.indexOf("Pale ") >= 0) {
                colorDiv.classList.add('round-color', 'my-1', 'mx-1')
                colorDiv.style.backgroundColor = "#cfa567"
                teddy.appendChild(colorDiv)
            } else if (color.indexOf("Dark ") >= 0) {
                colorDiv.classList.add('round-color', 'my-1', 'mx-1')
                colorDiv.style.backgroundColor = "#4d3d27"
                teddy.appendChild(colorDiv)
            } else {
                colorDiv.classList.add('round-color', 'my-1', 'mx-1')
                colorDiv.style.backgroundColor = color.toLowerCase()
                teddy.appendChild(colorDiv)
            }
        }
    }
}

